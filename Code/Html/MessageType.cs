﻿using System;

namespace CadastroPessoa.Code.Html
{
    public enum MessageType : int
    {
        Info = 0,
        Warning = 1,
        Success = 2,
        Error = 3
    }

    public static class MessageTypeExtension
    {
        public static string GetBootstrapAlertEquivalent(this MessageType msgType)
        {
            switch (msgType)
            {
                case MessageType.Info:
                    return "alert-info";
                case MessageType.Warning:
                    return "alert-warning";
                case MessageType.Success:
                    return "alert-success";
                case MessageType.Error:
                    return "alert-danger";
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
