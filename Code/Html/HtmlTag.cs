﻿using Microsoft.AspNetCore.Html;
using System;
using System.Reflection;
using System.Text;

namespace CadastroPessoa.Code.Html
{
    public class HtmlTag : IDisposable
    {
        private IHtmlContentBuilder content;
        private string tag;

        public HtmlTag(IHtmlContentBuilder content, string tag, object htmlAttributes = null)
        {
            this.content = content;
            this.tag = tag.Trim(' ', '<', '>');
            content.AppendHtmlLine($"<{this.tag}{GetAttributes(htmlAttributes)}>");
        }

        private object GetAttributes(object htmlAttributes)
        {
            if (htmlAttributes == null)
                return null;

            var properties = htmlAttributes.GetType().GetProperties();

            StringBuilder sb = new StringBuilder();
            foreach (var prop in properties)
                sb.Append(' ')
                    .Append(GetAttrName(prop.Name))
                    .Append("=\"").Append(prop.GetValue(htmlAttributes)).Append('"');

            return sb.ToString();
        }

        private string GetAttrName(string name)
            => name
            .Replace("_", "-");

        public void Dispose() => content.AppendHtmlLine($"</{tag}>");
    }
}
