﻿using System;

namespace CadastroPessoa.Code.Html
{
    public class ColumnDefinition
    {
        public string Name { get; set; }

        public Func<object, string> ValueParser { get; set; }

        public string Format { get; set; }

        public string Property { get; private set; }

        public ColumnDefinition(string property, string columnName, string format)
            : this(property, columnName)
        {
            Format = format;
        }

        public ColumnDefinition(string property, string columnName, Func<object, string> valueParser)
            : this(property, columnName)
        {
            ValueParser = valueParser;
        }

        public ColumnDefinition(string property, string columnName)
        {
            Property = property;
            Name = columnName;
        }
    }
}
