﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CadastroPessoa.Code.Html
{
    public static class HtmlExtension
    {
        /// <summary>
        /// Cria uma tabela simples utilizando as classes do Bootstrap
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="html"></param>
        /// <param name="id"></param>
        /// <param name="columns">Definição das colunas à serem exibidas</param>
        /// <param name="values">Lista com os valores para exibição</param>
        /// <returns></returns>
        public static IHtmlContent BootstrapTable<TModel, TValue>(this IHtmlHelper<TModel> html, string id,
            IEnumerable<ColumnDefinition> columns,
            IEnumerable<TValue> values)
        {
            var htmlContent = new HtmlContentBuilder();

            using (htmlContent.Tag("div", new { @class = "table-responsive" })) //https://github.com/twbs/bootlint/wiki/E027
            using (htmlContent.Tag("table", new { id, @class = "table table-bordered table-hover table-stripped" }))
            {
                using (htmlContent.Tag("thead"))
                    foreach (var column in columns)
                    {
                        using (htmlContent.Tag("th"))
                            htmlContent.AppendHtmlLine(html.Encode(column.Name));
                    }

                using (htmlContent.Tag("tbody"))
                    foreach (var valueObj in values)
                    {
                        using (htmlContent.Tag("tr"))
                        {
                            foreach (var column in columns)
                            {
                                object value = typeof(TValue)
                                    .GetProperty(column.Property)
                                    .GetValue(valueObj);

                                if (value != null)
                                    if (column.ValueParser != null)
                                        value = column.ValueParser(value);
                                    else if (!string.IsNullOrWhiteSpace(column.Format) &&
                                        value is IFormattable)
                                        value = (value as IFormattable).ToString(column.Format, null);

                                using (htmlContent.Tag("td"))
                                    if (value is IHtmlContent)
                                        htmlContent.AppendHtml((IHtmlContent)value);
                                    else
                                        htmlContent.AppendHtmlLine(html.Encode(value));
                            }
                        }
                    }
            }

            return htmlContent;
        }

        public static HtmlTag Tag(this IHtmlContentBuilder content, string tag, object htmlAttributes = null)
            => new HtmlTag(content, tag, htmlAttributes);
    }
}
