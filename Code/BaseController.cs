﻿using CadastroPessoa.Data;
using Microsoft.AspNetCore.Mvc;

namespace CadastroPessoa.Controllers
{
    public class BaseController : Controller
    {
        protected ApplicationContext context;

        public BaseController(ApplicationContext context)
        {
            this.context = context;
        }
    }
}
