﻿using CadastroPessoa.Data;
using CadastroPessoa.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CadastroPessoa.Code
{
    public static class DbContextExtensions
    {
        #region Genérico
        public static List<T> GetListFromDb<T>(
            this ApplicationContext context,
            int page = 0,
            int count = 15,
            string filterBy = null,
            string filterWith = null)
            where T : class
        {
            IEnumerable<T> list = context.SetWithNavegationProps<T>();

            //filtro
            if (!string.IsNullOrWhiteSpace(filterBy) && !string.IsNullOrWhiteSpace(filterWith))
            {
                var predicate = FilterHelper.GetFilterFromString<T>(filterBy, filterWith);
                list = list.Where(predicate);
            }

            //paginação
            list = list
                .Skip(page * count)
                .Take(count);

            return list.ToList();
        }

        public static IQueryable<T> SetWithNavegationProps<T>(this ApplicationContext context)
            where T : class
        {
            var db = context.Set<T>();

            //busca todas propriedades virtuais
            var navProps = typeof(T).GetProperties().
                 Where(p => p.GetGetMethod().IsVirtual).ToArray();

            if (navProps == null || navProps.Length == 0)
                return db;

            //pega a definicao dos métodos à serem utilizados
            IQueryable<T> query = db;
            MethodInfo includeMethod = typeof(EntityFrameworkQueryableExtensions).GetMethod(nameof(EntityFrameworkQueryableExtensions.Include)),
                lamdaCreateMethod = typeof(Expression).GetGenericMethod(nameof(Expression.Lambda), new Type[2] { typeof(Expression), typeof(IEnumerable<ParameterExpression>) });

            Type funcType = typeof(Func<,>);
            //inicia a criacao da expressão à ser passada para o Include
            ParameterExpression argParam = Expression.Parameter(typeof(T), "obj");

            //basicamente isso seria como colocar um include atras do outro. só q automático
            foreach (var nav in navProps)
            {
                Expression nameProperty = Expression.Property(argParam, nav);

                MethodInfo genLambdaCreate = lamdaCreateMethod.MakeGenericMethod(funcType.MakeGenericType(typeof(T), nav.PropertyType)),
                    genIncludeMethod = includeMethod.MakeGenericMethod(typeof(T), nav.PropertyType);

                //cria a expressão generica
                var expression = genLambdaCreate.Invoke(null, new object[2] { nameProperty, new ParameterExpression[1] { argParam } });

                object returnObj = genIncludeMethod.Invoke(null, new object[2] { db, expression });

                query = (IQueryable<T>)returnObj;
            }

            return query;
        }
        #endregion

        #region Pessoa
        public static Person GetPerson(this ApplicationContext context, string cpf)
            => context.People
            .Include(p => p.Telephones)
            .FirstOrDefault(p => p.Cpf == cpf);
        #endregion

        #region Configurações
        public static Configuration GetConfig(this ApplicationContext context, string configName)
            => context.Configurations
            .FirstOrDefault(c => c.Name == configName);

        public static Configuration SetConfig(this ApplicationContext context, string configName, string value)
        {
            var config = context.GetConfig(configName);

            if (config == null)
            {
                config = new Configuration()
                {
                    Name = configName,
                    Value = value
                };
                context.Configurations.Add(config);
            }
            else
                config.Value = value;

            context.SaveChanges();
            return config;
        }
        #endregion
    }
}
