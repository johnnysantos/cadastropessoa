﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CadastroPessoa.Code
{
    public class SimpleTypeComparer : IEqualityComparer<Type>
    {
        public bool Equals(Type x, Type y)
            => x.Namespace == y.Namespace
            && x.Name == y.Name;

        public int GetHashCode(Type obj) => obj.GetHashCode();
    }

    public static class TypeExtensions
    {
        public static MethodInfo GetGenericMethod(this Type type, string name, Type[] parameterTypes)
        {
            return type.GetMethods().FirstOrDefault(m => m.Name == name && m.GetParameters().Select(p => p.ParameterType).SequenceEqual(parameterTypes, new SimpleTypeComparer()));
        }
    }
}
