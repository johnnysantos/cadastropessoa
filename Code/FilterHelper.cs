﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;

namespace CadastroPessoa.Code
{
    /// <summary>
    /// Classe para auxílio na criação de restrições ou ordenações
    /// </summary>
    public static class FilterHelper
    {
        /// <summary>
        /// Cria um filtro baseado na propriedade informada
        /// Ex: obj.{propriedade} == {valor}
        /// </summary>
        /// <typeparam name="T">Tipo do objeto à ser filtrado</typeparam>
        /// <param name="property">Nome da propriedade pública existente no tipo informado.</param>
        /// <param name="value">valor à ser igualado</param>
        public static Func<T, bool> GetFilterFromString<T>(string property, object value)
        {
            //cria a expressão para 'buscar' a propriedade
            ParameterExpression argParam = Expression.Parameter(typeof(T), "obj");
            Expression nameProperty = Expression.Property(argParam, property);

            Type propType = typeof(T).GetProperty(property).PropertyType;
            if (propType == typeof(DateTime))
                nameProperty = Expression.Property(nameProperty, nameof(DateTime.Date));
            value = GetConvertedValue(value, propType);

            //cria uma expressão constante com o valor informado
            var constant = Expression.Constant(value, propType);

            //finalmente a expressão de igualdade é criada
            Expression e1 = Expression.Equal(nameProperty, constant);

            return Expression.Lambda<Func<T, bool>>(e1, argParam)
                .Compile();
        }

        private static object GetConvertedValue(object filterWith, Type type)
        {
            object value = filterWith;

            if (type == typeof(DateTime))
                value = DateTime.Parse(filterWith.ToString(), new CultureInfo("pt-br"));
            else if (type != typeof(string))
                value = Convert.ChangeType(filterWith, type);

            return value;
        }
    }
}
