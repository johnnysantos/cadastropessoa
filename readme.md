# Bem vindo ao cadastro de Pessoas

#### Definições

Cadastro de pessoas que irá rodar em clientes de SC e do PR.
A UF do cliente, onde o sistema estará rodando é definida em uma configuração.
O cadastro de pessoa deverá conter os seguintes campos:

 - Nome
 - CPF
 - Data/hora de cadastro
 - Data de nascimento
 - Telefones. O número de telefones é variável.

#### Regras:

Caso o cliente seja de SC, também é necessário cadastrar o RG.
Caso seja do PR, não deixar cadastrar uma pessoa com menos de 18 anos.
Criar também um relatório de clientes cadastrados (pode apenas ser gerado em tela), com filtros por nome, data de cadastro e data de nascimento.
Questões tecnológicas/frameworks, ficam a sua escolha (sugerimos você utilizar o que mais conhece).

# Pré-requisitos

Você deverá ter instalado os seguintes itens.

 - [dotnet](https://www.microsoft.com/net/core)
 - [nodejs](https://nodejs.org/en/)

# Executar

Você deverá executar os seguintes comandos:
```
$ bower install
$ dotnet restore
$ dotnet run
```

Pronto :+1: