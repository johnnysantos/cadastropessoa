var QueryStringCollection = function QueryStringCollection(queryString) {
    this.toString = function toString() {
        var str = '?';
        for (var obj in this) {
            if (typeof this[obj] === 'string' || typeof this[obj] === 'number')
                str += obj + '=' + encodeURIComponent(this[obj]) + '&';
        }
        return str.replace(/&$/, '');
    };

    if (queryString) {
        queryString = queryString.substr(1);
        var pair = queryString.split('&'),
            val = [];
        for (var i = 0; i < pair.length; i++) {
            val = pair[i].split('=');
            this[val[0]] = decodeURIComponent(val[1] || '');
        }
    }
}

jQuery(function ($) {
    $('[data-toggle="tooltip"]').tooltip();

    $('.btn-filter').click(function (e) {
        var filter = $('#filter-box');
        if (filter.css('display') === "none") {
            filter.show(500);
        } else {
            filter.hide(500);
        }
    });

    var queryString = new QueryStringCollection(window.location.search);
    $('[data-page]').each(function (i, o) {
        val = $(o).data('page');
        var actualPage = Number.parseInt(queryString['page'] || 0);
        if (val === 'proximo')
            queryString['page'] = actualPage + 1;
        else if (val === 'anterior')
            queryString['page'] = actualPage - 1;

        if (queryString['page'] > -1)
            o.href = queryString.toString();
        else
            $(o).addClass('disabled');

        queryString['page'] = actualPage;
    });

    $('#filterBy').change(function (e) {
        var selectedOption = e.currentTarget.selectedOptions[0];
        var textInput = $('#filterWith');
        if ($(selectedOption).is('[datetype]'))
            textInput.datetimepicker({ locale: 'pt-br', format: 'L' });
        else
            textInput.data('DateTimePicker').destroy();
    });

    $('[disabled]').each(function (i, obj) {
        obj.disabled = false;
    });
});