﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CadastroPessoa.Models
{
    public class Telephone
    {
        private const string minLenMsg = "O campo '{0}' deveria ter no mínimo {1} caracteres.",
            maxLenMsg = "O campo '{0}' deveria ter no máximo {1} caracteres.",
            requiredMsg = "O campo '{0}' é obrigatório.",
            onlyNumbers = "O {0} deve conter apenas números.";

        [Required(ErrorMessage = requiredMsg)]
        [RegularExpression(@"^\d{2,3}$", ErrorMessage = "O DDD deve conter apenas números.")]
        [MinLength(2, ErrorMessage = minLenMsg), MaxLength(3, ErrorMessage = maxLenMsg)]
        public string DDD { get; set; }

        [Key]
        [Display(Name = "Número")]
        [Required(ErrorMessage = requiredMsg)]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"\d+", ErrorMessage = onlyNumbers)]
        [MinLength(8, ErrorMessage = minLenMsg), MaxLength(10, ErrorMessage = maxLenMsg)]
        public string Number { get; set; }

        public string PersonCpf { get; set; }

        [JsonIgnore]
        public virtual Person Person { get; set; }
    }
}
