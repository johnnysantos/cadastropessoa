﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CadastroPessoa.Models
{
    public class Person
    {
        private const string minLenMsg = "O campo '{0}' deveria ter no mínimo {1} caracteres.",
            maxLenMsg = "O campo '{0}' deveria ter no máximo {1} caracteres.",
            requiredMsg = "O campo '{0}' é obrigatório.",
            onlyNumbers = "O {0} deve conter apenas números.";

        [Required(ErrorMessage = requiredMsg)]
        [MinLength(5, ErrorMessage = minLenMsg), MaxLength(150, ErrorMessage = maxLenMsg)]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Key]
        [Required(ErrorMessage = requiredMsg)]
        [MinLength(11, ErrorMessage = minLenMsg), MaxLength(11, ErrorMessage = maxLenMsg)]
        [RegularExpression(@"\d+", ErrorMessage = onlyNumbers)]
        public string Cpf { get; set; }

        [MinLength(6, ErrorMessage = minLenMsg), MaxLength(11, ErrorMessage = maxLenMsg)]
        [RegularExpression(@"\d+", ErrorMessage = onlyNumbers)]
        public string Rg { get; set; }

        [UIHint("UF")]
        [Required(ErrorMessage = requiredMsg)]
        [MinLength(2, ErrorMessage = minLenMsg), MaxLength(3, ErrorMessage = maxLenMsg)]
        public string Uf { get; set; }

        [Required(ErrorMessage = requiredMsg)]
        [DataType(DataType.Date, ErrorMessage = "O campo {0} está inválido.")]
        [Display(Name = "Data de Nascimento")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = requiredMsg)]
        [DataType(DataType.Date)]
        public DateTime RegistrationDate { get; set; }

        [JsonIgnore]
        public virtual ICollection<Telephone> Telephones { get; set; } = new HashSet<Telephone>();
    }
}
