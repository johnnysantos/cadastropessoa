﻿using System.Collections.Generic;

namespace CadastroPessoa.Models
{
    public class TelephoneAddModelView
    {
        public string Cpf { get; set; }

        public Telephone Telephone { get; set; }

        public ICollection<Telephone> Telephones { get; set; }

        public TelephoneAddModelView()
        {
        }

        public TelephoneAddModelView(string cpf, ICollection<Telephone> telephones)
        {
            this.Cpf = cpf;
            this.Telephones = telephones;
            this.Telephone = new Telephone();
        }
    }
}
