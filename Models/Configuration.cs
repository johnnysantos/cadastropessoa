﻿using System.ComponentModel.DataAnnotations;

namespace CadastroPessoa.Models
{
    public class Configuration
    {
        public const string UF_KEY = "operacao-uf";

        [Key]
        [Required]
        [MinLength(2), MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(-1)]
        public string Value { get; set; }
    }
}