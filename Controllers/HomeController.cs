using CadastroPessoa.Data;
using Microsoft.AspNetCore.Mvc;

namespace CadastroPessoa.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ApplicationContext context) : base(context)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
