using CadastroPessoa.Code;
using CadastroPessoa.Code.Html;
using CadastroPessoa.Data;
using CadastroPessoa.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroPessoa.Controllers
{
    public class RegisterController : BaseController
    {
        public RegisterController(ApplicationContext context)
            : base(context)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["UF"] = context.GetConfig(Configuration.UF_KEY)?.Value;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(Person model)
        {
            Configuration config = context.GetConfig(Configuration.UF_KEY);
            ViewData["UF"] = config?.Value;

            model.RegistrationDate = DateTime.Now;
            model.Cpf = model.Cpf?
                .Replace("-", string.Empty)
                .Replace(".", string.Empty)
                .Replace("/", string.Empty);

            if (IsPersonValid(config, model))
            {
                context.People.Add(model);
                try
                {
                    await context.SaveChangesAsync();
                    return RedirectToAction(nameof(AddTel), new { cpf = model.Cpf });
                }
                catch (Exception e)
                {
                    ViewData["Message"] = e.Message;
                    ViewData["MessageType"] = MessageType.Error;
                }
            }
            else
            {
                GetErrorMsgs();
            }

            return View(model);
        }

        private void GetErrorMsgs()
        {
            var error = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();

            if (error != null)
            {
                ViewData["Message"] = error.ErrorMessage;
                ViewData["MessageType"] = MessageType.Warning;
            }
        }

        private bool IsPersonValid(Configuration config, Person model)
        {
            if (ModelState.IsValid)
            {
                if (config == null)
                {
                    ModelState.AddModelError(nameof(model.Rg), "UF do cliente n�o foi preenchida.");
                }
                else if (config.Value == "SC" && string.IsNullOrWhiteSpace(model.Rg))
                {
                    ModelState.AddModelError(nameof(model.Rg), "O campo RG � obrigat�rio.");
                }
                else if (config.Value == "PR" && model.DateOfBirth < DateTime.Today.AddYears(-18))
                {
                    ModelState.AddModelError(nameof(model.DateOfBirth), "� obrigat�rio ter mais de dezoito anos para se cadastrar.");
                }

                return ModelState.IsValid;
            }
            return false;
        }

        [HttpGet]
        public IActionResult AddTel(string cpf)
        {
            var person = context.GetPerson(cpf);
            if (person == null)
                return RedirectToAction(nameof(RegisterController.Index));
            return View(new TelephoneAddModelView(person.Cpf, person.Telephones));
        }

        [HttpPost]
        public IActionResult AddTel(TelephoneAddModelView model)
        {
            var person = context.GetPerson(model.Cpf);

            if (ModelState.IsValid)
            {
                person.Telephones.Add(model.Telephone);
                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(nameof(TelephoneAddModelView.Telephone), e.Message);
                }
            }
            GetErrorMsgs();

            return View(new TelephoneAddModelView(person.Cpf, person.Telephones));
        }

        [HttpGet]
        public IActionResult Config(string uf)
        {
            if (!string.IsNullOrWhiteSpace(uf))
            {
                context.SetConfig(Configuration.UF_KEY, uf);
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            return View();
        }
    }
}
