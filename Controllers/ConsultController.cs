﻿using CadastroPessoa.Code;
using CadastroPessoa.Data;
using CadastroPessoa.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CadastroPessoa.Controllers
{
    public class ConsultController : BaseController
    {
        public ConsultController(ApplicationContext context)
            : base(context)
        {
        }

        /// <summary>
        /// View principal
        /// </summary>
        public IActionResult Index(
            [FromQuery]int page = 0,
            [FromQuery]int count = 15,
            [FromQuery]string filterBy = null,
            [FromQuery]string filterWith = null)
        {
            List<Person> model = context.GetListFromDb<Person>(page, count, filterBy, filterWith);

            return View(model);
        }
    }
}
