using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace CadastroPessoa
{
    public class Program
    {
        /// <summary>
        /// Configura��es do programa
        /// </summary>
        /// <remarks>
        /// Movi as configura��es pra c� para ficar mais legivel
        /// 
        /// Program.Configuration ?
        /// Startup.Configuration ?
        /// </remarks>
        public static IConfigurationRoot Configuration { get; internal set; }

        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            var host = new WebHostBuilder()
                .UseConfiguration(config)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
