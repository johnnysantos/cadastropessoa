using CadastroPessoa.Models;
using Microsoft.EntityFrameworkCore;

namespace CadastroPessoa.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions options)
            : base(options)
        {
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connection = Program.Configuration.GetSection("ConnectionString").Value;
            optionsBuilder.UseSqlite(connection,
                sqliteOptionsAction: sqlOpt =>
                {
                    //� melhor sem underlines
                    sqlOpt.MigrationsHistoryTable("EfMigrations");
                });
        }

        public virtual DbSet<Person> People { get; set; }

        public virtual DbSet<Telephone> Telephones { get; set; }

        public virtual DbSet<Configuration> Configurations { get; set; }
    }
}